FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["MapDiscover.Services.PoiAggregator.csproj", ""]
RUN dotnet restore "./MapDiscover.Services.PoiAggregator.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "MapDiscover.Services.PoiAggregator.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "MapDiscover.Services.PoiAggregator.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "MapDiscover.Services.PoiAggregator.dll"]