using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using MapDiscover;
using Microsoft.Extensions.Logging;
using static MapDiscover.PoiAggregatorService;

namespace MapDiscover.Services.PoiAggregator
{
    public class PoiAggregatorService : PoiAggregatorServiceBase
    {
        private readonly ILogger<PoiAggregatorService> logger;

        public PoiAggregatorService(ILogger<PoiAggregatorService> logger)
        {
            this.logger = logger;
        }

        public override Task<Empty> Delete(PoiAggregatorDeleteRequest request, ServerCallContext context)
        {
            return base.Delete(request, context);
        }

        public override Task<Empty> Upsert(PoiAggregatorUpsertRequest request, ServerCallContext context)
        {
            return base.Upsert(request, context);
        }
    }
}
